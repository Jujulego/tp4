$(document).ready(function() {
    // Elements
    const nbparts     = $(".nb-parts");
    const infosclient = $(".infos-client");
    const pizzapict   = $("<span class=\"pizza-pict\"></span>");
    const price       = $(".stick-right p");

    let extra_weather = 0;

    // Fonctions
    function updatepizzapicts() {
        $(".pizza-pict", nbparts).remove();

        const parts = $("input", nbparts).val();
        const mod = parts % 6;

        for (let i = 0; i < (parts - mod) / 6; ++i) {
            nbparts.append(pizzapict.clone().addClass("pizza-6"));
        }

        if (mod !== 0) {
            nbparts.append(pizzapict.clone().addClass(`pizza-${mod}`));
        }
    }

    function updateprice() {
        let p = 0;

        // Price of parts
        const nb = $("input", nbparts).val() / 6;
        p = $("input[name='type']:checked").attr("data-price") * nb;

        // Options
        p += ($("input[name='pate']:checked").attr("data-price") * nb) || 0;
        $("input[name='extra']:checked").each((i, e) => p += $(e).attr("data-price") * nb);
        p += extra_weather;

        price.text(`${Math.round(p * 100) / 100} €`);
    }

    function getweather() {
        //                                                             v----------v => temp en °C
        $.get("https://api.openweathermap.org/data/2.5/weather?q=paris&units=metric&appid=768a35a09a1701be84498950a95e7cf5")
            .done((data) => {
                if (data.main.temp <= 0) {
                    extra_weather = 5;
                    updateprice();

                    $("<p><small>+5 € pour le livreur, c'est qu'il fait froid dehors !</small></p>").insertAfter(price);
                }
            });
    }

    // Initialisation
    updatepizzapicts();
    updateprice();

    getweather();

    // Events
    // - descriptions
    $(".pizza-type label").hover(
        function() { $(".description", this).show() },
        function() { $(".description", this).hide() }
    );

    // - prix
    $("input[data-price]").change(updateprice);
    $("input", nbparts).change(function() {
        updatepizzapicts();
        updateprice();
    });

    // Boutons
    $(".next-step").click(function() {
        $(this).hide();
        infosclient.show()
    });
    $(".add").click(function() {
        $("<br /><input type=\"text\"/>").insertBefore(this);
    });
    $(".done").click(function() {
        const prenom = $(".type:first input", infosclient).val();

        $(".main").empty()
            .append($(`<p class="text-center">Merci ${prenom} ! Votre commande sera livrée dans 15 minutes</p>`))
    });
});